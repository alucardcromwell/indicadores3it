export interface Title{
  romanji: string, english?: string, native?: string
}
export interface Anime{
  id: number, title: Title
}

export interface AnimeList{
  animes: Anime[]
}

export interface Indicador{
  codigo: string, nombre: string, unidad_medida: string, fecha: string, valor: number
}

export interface IndicadoresSimple{
  indicadores: Indicador[]
}

export interface Serie{
  fecha: string, valor: number
}

export interface InterfazDetalleIndicador{
  version: string, autor: string, codigo: string, nombre: string, unidad_medida: string, serie: Serie[]
}

export interface Indicadores{
  version: string,
  autor: string,
  fecha: string,
  uf: Indicador,
  ivp: Indicador,
  dolar: Indicador,
  dolar_intercambio: Indicador,
  euro: Indicador,
  ipc: Indicador,
  utm: Indicador,
  imacec: Indicador,
  tpm: Indicador,
  libra_cobre: Indicador,
  tasa_desempleo: Indicador,
  bitcoin: Indicador

}
