export interface Poi{
  id: number, placeId: string, name: string, latitude: string, longitude: string, id_local: number, id_empresa: number
}
export interface Pois{
  pois: Poi[]
}
export interface Position{
  latitude: number, longitude: number, latitudeDelta: number, longitudeDelta: number
}
