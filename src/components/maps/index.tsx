import React, { useState, useEffect } from 'react';
import { StyleSheet, Dimensions, View, Text, Alert, FlatList, TouchableOpacity, Modal } from 'react-native';
import { FontAwesome, FontAwesome5 } from '@expo/vector-icons';
import MapView, { Marker } from 'react-native-maps';
import *  as Location from 'expo-location';
import * as Permissions from 'expo-permissions';
import API from '@constants/API';
import {GEOLOCATION_OPTIONS} from '@constants/GeoLocation';
import {Poi, Pois, Position} from '@constants/Interfaces';

const Maps: React.FC<Pois.pois> = (props) => {
    const [pois, setPois] = React.useState<Pois.pois>([]);
    const [location, setLocation] = React.useState<Position>(null);
    const [errorMsg, setErrorMsg] = React.useState(null);
    let watchId = null;

    useEffect(() => {
      (async () => {
        let { status } = await Location.requestPermissionsAsync();
        if (status !== 'granted') {
          setErrorMsg('Permission to access location was denied');
          return;
        }
        watchId = await Location.watchPositionAsync(GEOLOCATION_OPTIONS, updatePosition)
      })();

      const updatePosition = (newPosition: Location.LocationObject) => {
        const newPos = (({latitude, longitude}) => ({ latitude, longitude}))(newPosition.coords);
        if(location === null){
          setLocation({latitude: newPos.latitude, longitude: newPos.longitude, latitudeDelta: 0.0922, longitudeDelta: 0.0421});  
        }else{
          const actual = (({latitude, longitude}) => ({ latitude, longitude}))(location);
          if(!(newPos.latitude == actual.latitude && newPos.longitude == actual.longitude)){
            setLocation({latitude: newPos.latitude, longitude: newPos.longitude, latitudeDelta: 0.0922, longitudeDelta: 0.0421});  
          }
        }
      }
      const fetchData = async() => {
        fetch(API.full_uri, {
          method: 'GET',
          headers: {
            Accept: 'application/json',
          }
        })
        .then((response) => response.json())
        .then((responseJson) => {
          setPois(responseJson);
        }).catch((error) => {
          console.warn('Request Failed: ', error);
        });
      }
      fetchData();
    },[]);
    const onPressMarker = async(marker: Poi) => {
      console.log(marker);
    }
      return (
        <View style={styles.container}>
          <MapView style={styles.map}
            region={location}
            showsUserLocation={true}
            followsUserLocation={true}
            showsMyLocationButton={true}
            loadingEnabled={true}
          >
          {pois.map((marker: Poi)=> (
                <Marker
                  coordinate={{ latitude: parseFloat(marker.latitude), longitude: parseFloat(marker.longitude) }}
                  title={marker.name}
                  identifier={marker.placeId}
                  key={marker.id}
                  onPress={(e) => onPressMarker(marker)}
                />
          ))}
          </MapView>
        </View>
      );
      
      
  }
  export default Maps;
  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
    },
    title: {
      fontSize: 20,
      fontWeight: 'bold',
    },
    separator: {
      marginVertical: 30,
      height: 1,
      width: '80%',
    },
    map: {
      width: Dimensions.get('window').width,
      height: Dimensions.get('window').height,
    },
  });
  