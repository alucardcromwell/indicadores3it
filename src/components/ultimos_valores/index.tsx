import React, { useState, useEffect, SetStateAction, Dispatch } from 'react';
import { Text, View, StyleSheet, Dimensions, TextInput, FlatList, TouchableOpacity, SafeAreaView  } from 'react-native';
import Constants from 'expo-constants';
import API from '@constants/API';
import { Position } from '@constants/Interfaces';

const UltimosValores = ({codigo}: string) => {
  const [values, setValues] = React.useState([]);
  useEffect(() => {
    const fetchApi = async () => {
      fetch(API.api_uri+"/"+codigo, {
        method: 'GET',
        headers: {
          Accept: 'application/json',
        },
      })
        .then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson);
          setValues(responseJson.serie);
          //dispatch({ type: 'set', payload: responseJson });
        }).catch((error) => {
          console.warn('Request Failed: ', error);
        });
    }
    fetchApi();
  }, []);

  

  const renderItem = ({ item }) => {
    return (
      <View style={{flex:1, borderBottomWidth: 1, borderBottomColor: "lightgray",flexDirection: "row", marginBottom: 6, paddingBottom: 6}}>
        <View style={{flex:1}}><Text style={{fontSize: 16, color: "lightblue"}}>{item.fecha.substring(0,10)}</Text></View>
        <View style={{flex:1}}><Text style={{fontSize: 16}}>$ {item.valor.toLocaleString()}</Text></View>
      </View>
    );
  };
  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        data={values}
        renderItem={renderItem}
        keyExtractor={(item) => item.fecha}
      />
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    paddingTop: Constants.statusBarHeight + 10,
    backgroundColor: '#ecf0f1',
  },
  input: {
    paddingLeft: 10, height: 40, borderColor: "#5564eb", borderWidth: 1.5, borderRadius: 6, color: "#5564eb" 
  },
  item: {
    padding: 10,
    marginVertical: 4,
    marginHorizontal: 4,
    borderBottomWidth: 0.8,
    borderBottomColor: "#5564eb"
  },
  title: {
    fontSize: 12,
  },
});

export default UltimosValores;