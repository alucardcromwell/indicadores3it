import React, { useState, useEffect, SetStateAction, Dispatch } from 'react';
import { Text, View, StyleSheet, Dimensions, TextInput, FlatList, TouchableOpacity, SafeAreaView } from 'react-native';
import Constants from 'expo-constants';
import { LineChart } from "react-native-chart-kit";
import API from '@constants/API';
import { InterfazDetalleIndicador } from '@constants/Interfaces';

const screenWidth = Dimensions.get("window").width-39;

const DetalleIndicador = ({ codigo }: string) => {
  const [detalle, setDetalle] = React.useState<InterfazDetalleIndicador>({ nombre: "", unidad_medida: "", serie: [] });
  let labels = [];
  let data = [];
  if(detalle.serie.length > 0){
    for (let i = 0; i < 10; i++) {
      labels.push(detalle.serie[i].fecha.substring(0,10));
      data.push(detalle.serie[i].valor);
    }
  }
  //const [values, setValues] = React.useState([]);
  useEffect(() => {
    const fetchApi = async () => {
      fetch(API.api_uri + "/" + codigo, {
        method: 'GET',
        headers: {
          Accept: 'application/json',
        },
      })
        .then((response) => response.json())
        .then((responseJson) => {
          setDetalle(responseJson);
        }).catch((error) => {
          console.warn('Request Failed: ', error);
        });
    }
    fetchApi();
  }, []);

  if (detalle.serie.length > 0) {
    return (
      <View style={styles.container}>
        <View style={{ flex: 2, flexDirection: "row", alignItems: "center", justifyContent: "center" }}><Text style={{ fontSize: 24, fontWeight: "bold", color: "lightblue" }}>{detalle.serie[0].valor}</Text><Text style={{ color: "lightblue", fontWeight: "bold" }}>  {detalle.unidad_medida}</Text></View>
        <View style={{ flex: 1, flexDirection: "row" }}>
          <View style={{ flex: 1 }}><Text>Nombre</Text></View>
          <View style={{ flex: 1 }}><Text>{detalle.nombre}</Text></View>
        </View>
        <View style={{ flex: 1, flexDirection: "row" }}>
          <View style={{ flex: 1 }}><Text>Fecha</Text></View>
          <View style={{ flex: 1 }}><Text>{detalle.serie[0].fecha.substring(0, 10)}</Text></View>
        </View>
        <View style={{ flex: 1, flexDirection: "row" }}>
          <View style={{ flex: 1 }}><Text>Unidad de Medida</Text></View>
          <View style={{ flex: 1 }}><Text>{detalle.unidad_medida}</Text></View>
        </View>
        <View style={{ flex: 5 }}>
          <LineChart
            data={{
              labels: labels,
              datasets: [
                {
                  data: data
                }
              ]
            }}
            width={screenWidth} // from react-native
            height={220}
            yAxisLabel="$"
            yAxisSuffix="k"
            yAxisInterval={1} // optional, defaults to 1
            chartConfig={{
              backgroundColor: "#e26a00",
              backgroundGradientFrom: "#fb8c00",
              backgroundGradientTo: "#ffa726",
              decimalPlaces: 2, // optional, defaults to 2dp
              color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
              labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
              style: {
                borderRadius: 16
              },
              propsForDots: {
                r: "6",
                strokeWidth: "2",
                stroke: "#ffa726"
              }
            }}
            bezier
            style={{
              marginVertical: 8,
              borderRadius: 16
            }}
          />
        </View>
      </View>
    );
  } else {
    return (
      <View style={{ flex: 1, alignContent: "center", alignItems: "center", justifyContent: "center" }}>
        <Text>Cargando...</Text>
      </View>
    )
  }

};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    paddingLeft: 15,
    paddingTop: Constants.statusBarHeight + 10,
    backgroundColor: '#ecf0f1',
  },
  input: {
    paddingLeft: 10, height: 40, borderColor: "#5564eb", borderWidth: 1.5, borderRadius: 6, color: "#5564eb"
  },
  item: {
    padding: 10,
    marginVertical: 4,
    marginHorizontal: 4,
    borderBottomWidth: 0.8,
    borderBottomColor: "#5564eb"
  },
  title: {
    fontSize: 12,
  },
});

export default DetalleIndicador;