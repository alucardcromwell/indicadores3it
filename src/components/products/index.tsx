import React, { useState, useEffect, useCallback, useReducer } from 'react';
import { StyleSheet, Dimensions, View, Text, Alert, FlatList, TouchableOpacity, Modal, SafeAreaView, Pressable } from 'react-native';
import { FontAwesome, FontAwesome5 } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';
import API from '@constants/API';
import { AnimeList, Anime } from '@constants/Interfaces';

const query = `
query ($id: Int, $page: Int, $perPage: Int, $search: String) {
  Page (page: $page, perPage: $perPage) {
    pageInfo {
      total
      currentPage
      lastPage
      hasNextPage
      perPage
    }
    media (id: $id, search: $search) {
      id
      title {
        romaji
      }
    }
  }
}
`;

const Products: React.FC = () => {
  console.log("Entro Recien");
  const navigation = useNavigation();
  const [data, setData] = React.useState<AnimeList>({});
  useEffect(() => {
    console.log("Inicio efecto fetchApi");
    const fetchApi = async () => {
      fetch(API.api_uri, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            query: query,
        })
      })
        .then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson.data.Page.media);
          setData(responseJson.data.Page.media);

        }).catch((error) => {
          console.warn('Request Failed: ', error);
        });
    }
    fetchApi();
  },[]);

  const renderItem = ({ item }) => (
    <View style={styles.renderItem}>
      <View style={{ flex: 4 }}>
        <View style={{ flex: 1 }}>
            <Text style={styles.itemText}>{item.id}</Text>
        </View>
        <View style={{ flex: 1, paddingTop: 6 }}>
          <Text style={styles.titleText}>{item.title.romaji}</Text>
        </View>
      </View>
      <View style={{ flex: 1, flexDirection: "row", alignItems: "center" }}>
        <View style={{ flex: 1 }}>
            <FontAwesome5 name="info-circle" size={28} color="black" />
        </View>
        <View style={{ flex: 1 }}>
          <FontAwesome5 name="chevron-right" size={18} color="gray" />
        </View>
      </View>
    </View>
  );

  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        data={data}
        renderItem={renderItem}
        keyExtractor={item => item.id}
      />
    </SafeAreaView>

  )
}
export default Products;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 5,
    paddingLeft: 5,
    paddingRight: 5
  },
  renderItem: {
    flex: 1,
    flexDirection: "row",
    marginBottom: 6,
    paddingBottom: 8,
    borderBottomWidth: 1,
    borderColor: "gray"
  },
  firstItem: {
    flex: 1
  },
  secondItem: {
    flex: 4, flexDirection: "row", paddingTop: 5, paddingBottom: 10
  },
  itemText: {
    fontSize: 14,
    color: "red",
  },
  titleText: {
    color: "black",
    fontSize: 15.5,
    fontWeight: "700"
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  map: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },
});
