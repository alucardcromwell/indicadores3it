import * as React from 'react';
import { StyleSheet, Dimensions, View } from 'react-native';
import DetalleIndicador from '@components/detalle_indicador';

export default function DetalleIndicadorView({route, navigation}) {
  const { codigo } = route.params;
  return (
    <View style={styles.container}>
      <DetalleIndicador codigo={codigo}></DetalleIndicador>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1, padding: 5
  },
});
