import * as React from 'react';
import { StyleSheet, Dimensions, View } from 'react-native';
import Products from '@components/products';

export default function TabOneScreen() {
  return (
    <View style={styles.container}>
      <Products></Products>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1, padding: 5
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
});
