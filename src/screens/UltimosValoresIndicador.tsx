import * as React from 'react';
import { StyleSheet, Dimensions, View } from 'react-native';
import UltimosValores from '@components/ultimos_valores';

export default function UltimosValoresIndicador({route, navigation}) {
  const { codigo } = route.params;
  return (
    <View style={styles.container}>
      <UltimosValores codigo={codigo}></UltimosValores>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1, padding: 5
  },
});
